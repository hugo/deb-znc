#include <znc/znc.h>
#include <znc/User.h>
#include <znc/IRCNetwork.h>
#include <znc/Modules.h>

#include <sys/types.h>
#include <pwd.h>

class LysatorConfig : public CModule {
	public:
		MODCONSTRUCTOR(LysatorConfig) { }

	EModRet OnAddUser (CUser& user, CString& errStr) override {
		const CString& nick = user.GetNick(false);

		struct passwd* pw = getpwnam(nick.c_str());
		if (pw) {
			user.SetRealName(CString(pw->pw_gecos));
		}

		CIRCNetwork* network = user.AddNetwork("libera", errStr);
		if (! network) {
			return CONTINUE;
		}

		network->AddServer("irc.libera.chat");
		network->AddChan("#lysator", false);

		return CONTINUE;
	}


};

GLOBALMODULEDEFS(LysatorConfig, t_s("Loads #lysator for new users."))
